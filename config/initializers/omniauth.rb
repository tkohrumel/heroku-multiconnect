OmniAuth.config.logger = Rails.logger

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :developer unless Rails.env.production?
  provider :salesforce, ENV['MP_CONSUMER_KEY'], ENV['MP_CONSUMER_SECRET']
end