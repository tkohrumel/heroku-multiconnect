class AdminMailer < ApplicationMailer
  default from: 'tkohrumel@salesforce.com'

  def authorize_with_rails(admin)
    @admin = admin
    mail(to: @admin, subject: 'ACTION REQUIRED: Authorize Web Service for Salesforce Org')
  end

  def authenticate_heroku_connect(admin_email)
    mail(to: admin_email, subject: 'ACTION REQUIRED: Authenticate Heroku Connect for Your Salesforce Org')
  end
end