require 'httparty'

class ProvisionAndLinkConnectionWorker
  include Sidekiq::Worker

  def perform(sfdc_org_id, local_id)
    response = ::HerokuPlatformAPI.new.provision_hc_connection(sfdc_org_id)
    data = response.parsed_response
    org = Organization.find local_id
    org.connection_id = data["id"]

    if org.save
      logger.info "Organization #{org.id} now has a Heroku Connect connection, with connection id: #{org.connection_id}."

      logger.info "Linking HC connection: #{org.connection_id} for org: #{org.org_id} to Heroku account..." 
      ::HerokuConnectAPI.new.link_connection_to_account

      ConnectionConfigWorker.perform_in(2.minutes, org.connection_id, org.org_id, org.admin_email)
    else
      # TODO
    end
  end
end