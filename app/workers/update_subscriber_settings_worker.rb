class UpdateSubscriberSettingsWorker 
  include Sidekiq::Worker

  def perform(oauth_token, refresh_token, instance_url, connection_id, authorize_url)
    logger.info "Perfoming HTTP request to subscriber's Salesforce org to update custom setting with connection info" 
    force = UpdateSubscriberSettingsWorker.restforce_client(oauth_token, refresh_token, instance_url) 
    resp = force.post '/services/apexrest/HCPOC/AuthConnection', :connectionId => connection_id, :authorizeUrl => authorize_url
    logger.info resp
  end

  def self.restforce_client(oauth_token, refresh_token, instance_url)
    Restforce.new :oauth_token => oauth_token,
      :refresh_token => refresh_token,
      :instance_url => instance_url,
      :client_id => ENV['MP_CONSUMER_KEY'],
      :client_secret => ENV['MP_CONSUMER_SECRET']
  end
end