class ConfigConnectionJob < ActiveJob::Base
  queue_as :default

  def perform(connection_id, org_id)
    p "Performing ConfigConnectionJob..."
    # Do something later
    heroku_connect = ::HerokuConnectAPI.new
    response = heroku_connect.configure_db_key_and_schema_for_connection(connection_id, org_id)
    p "done. *Response: #{response}"
  end
end